import { Link } from "react-router-dom";
import { useMediaQuery } from "react-responsive";
import styles from "./Header.module.scss";
import { useState } from "react";

export function Header() {
  const [menuActive, setMenuActive] = useState(false);
  const isMobile = useMediaQuery({ query: "(max-width: 55em)" });
  // console.log(isMobile);

  function handleMenuActive() {
    setMenuActive((prevState) => !prevState);
  }

  console.log(menuActive);

  return (
    <header className={styles.header}>
      <nav className={styles["header__nav"]}>
        <div className={styles["header__nav-logo"]}>
          <Link to={"/"}>
            <img src="/images/logo_apple_musik.png" alt="Logo apel musik" />
          </Link>
        </div>

        {/* Hamburger menu */}

        {menuActive ? (
          <div
            className={`${styles.burger_menu} ${
              isMobile ? "" : styles.menu_hidden
            }`}
          >
            <button onClick={handleMenuActive}>
              <img src="/images/close_burger.svg" alt="burger menu" />
            </button>
          </div>
        ) : (
          <div
            className={`${styles.burger_menu} ${
              isMobile ? "" : styles.menu_hidden
            }`}
          >
            <button onClick={handleMenuActive}>
              <img src="/images/menu_burger.svg" alt="burger menu" />
            </button>
          </div>
        )}

        {/* ------------ */}

        {/* ---------- Menu Active when Burger Menu active ---------*/}

        {menuActive && (
          <ul
            className={`${styles.menu_active} ${
              menuActive ? "" : styles.menu_hidden
            }`}
          >
            <li>
              <Link to={"/register"} className={styles.register}>
                Daftar Sekarang
              </Link>
            </li>
            <li style={{ marginTop: "3em" }}>
              <Link to={"/login"} className={styles.login}>
                Masuk
              </Link>
            </li>
          </ul>
        )}

        {menuActive && <div className={styles.modal_active}></div>}

        {/* ------------------------------------ */}

        <ul
          className={`${styles["header__nav-link"]} ${
            isMobile ? styles.hidden : ""
          }`}
        >
          <li>
            <Link to={"/register"} className={styles.register}>
              Daftar Sekarang
            </Link>
          </li>
          <li>
            <Link to={"/login"} className={styles.login}>
              Masuk
            </Link>
          </li>
        </ul>
      </nav>
    </header>
  );
}
