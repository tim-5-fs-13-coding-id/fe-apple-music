import styles from "./DetailProduct.module.scss";

export function DetailProduct({ category, title, jadwal, price, image }) {
  return (
    <article className={styles.product}>
      <img
        src={`/images/${image}`}
        alt={title}
        className={styles.product__image}
      />
      <div className={styles.product__desc}>
        <h1>{category}</h1>
        <h2>{title}</h2>
        <h3>{jadwal}</h3>
        <h4>{price}</h4>
      </div>
    </article>
  );
}
