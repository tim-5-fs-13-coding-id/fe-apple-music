import styles from "./Footer.module.scss";

export function Footer() {
  return (
    <footer className={styles.footer}>
      <section className={styles.about}>
        <h1>Tentang</h1>
        <p>
          Sed ut perspiciatis unde omnis iste natus error sit voluptatem
          accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae
          ab illo inventore veritatis et quasi architecto beatae vitae dicta
          sunt explicabo.{" "}
        </p>
      </section>
      <section className={styles.product}>
        <h1>Produk</h1>
        <div>
          <ul>
            <li>Biola</li>
            <li>Gitar</li>
            <li>Drum</li>
          </ul>
          <ul>
            <li>Menyanyi</li>
            <li>Piano</li>
            <li>Saxophone</li>
          </ul>
        </div>
      </section>
      <section className={styles.contact}>
        <div className={styles.top}>
          <h1>Alamat</h1>
          <p>
            Sed ut perspiciatis unde omnis iste natus error sit voluptatem
            accusantium doloremque.
          </p>
        </div>
        <div className={styles.bottom}>
          <h1>Kontak</h1>
          <ul>
            <li>
              <a href="#">
                <img src="/images/phone_icon.svg" alt="Phone icon" />
              </a>
            </li>
            <li>
              <a href="#">
                <img src="/images/instagram_icon.svg" alt="Instagram icon" />
              </a>
            </li>
            <li>
              <a href="#">
                <img src="/images/youtube_icon.svg" alt="Youtube icon" />
              </a>
            </li>
            <li>
              <a href="#">
                <img src="/images/telegram_icon.svg" alt="Telegram icon" />
              </a>
            </li>
            <li>
              <a href="#">
                <img src="/images/email_icon.svg" alt="Email icon" />
              </a>
            </li>
          </ul>
        </div>
      </section>
    </footer>
  );
}
