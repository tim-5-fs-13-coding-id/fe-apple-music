import { Link } from "react-router-dom";
import styles from "./NavbarAuthenticate.module.scss";
import { useState } from "react";
import { useMediaQuery } from "react-responsive";

export function NavbarAuthenticate() {
  const [menuActive, setMenuActive] = useState(false);
  const isMobile = useMediaQuery({ query: "(max-width: 55em)" });
  // console.log(isMobile);

  function handleMenuActive() {
    setMenuActive((prevState) => !prevState);
  }

  console.log(menuActive);

  return (
    <nav className={styles.header}>
      <div className={styles.nav}>
        <div className={styles.nav__logo}>
          <img src="/images/logo_apple_musik.png" alt="Logo apel musik" />
        </div>

        {/* Hamburger menu */}

        {menuActive ? (
          <div
            className={`${styles.burger_menu} ${
              isMobile ? "" : styles.menu_hidden
            }`}
          >
            <button onClick={handleMenuActive}>
              <img src="/images/close_burger.svg" alt="burger menu" />
            </button>
          </div>
        ) : (
          <div
            className={`${styles.burger_menu} ${
              isMobile ? "" : styles.menu_hidden
            }`}
          >
            <button onClick={handleMenuActive}>
              <img src="/images/menu_burger.svg" alt="burger menu" />
            </button>
          </div>
        )}

        {/* ------------ */}

        {/* ---------- Menu Active when Burger Menu active ---------*/}

        {menuActive && (
          <ul
            className={`${styles.menu_active} ${
              menuActive ? "" : styles.menu_hidden
            }`}
          >
            <li>
              <Link
                to={"/user/cart"}
                href="#"
                className={styles.nav__link_cart}
              >
                <img src="/images/Cart.svg" alt="Cart Shop" />
              </Link>
            </li>
            <li>
              <Link to={"/"} href="#" className={styles.nav__link_kelas}>
                Kelasku
              </Link>
            </li>
            <li>
              <Link to={"/"} href="#" className={styles.nav__link_pembelian}>
                Pembelian
              </Link>
            </li>
            <li>
              <Link to={"/"} href="#" className={styles.nav__link_person}>
                <img src="/images/Person.svg" alt="Cart Shop" />
              </Link>
            </li>
            <li>
              <Link to={"/"} href="#" className={styles.nav__link_logout}>
                <img src="/images/Logout.svg" alt="Cart Shop" />
              </Link>
            </li>
          </ul>
        )}

        {menuActive && <div className={styles.modal_active}></div>}

        {/* ------------------------------------ */}

        <ul className={`${styles.nav__link} ${isMobile ? styles.hidden : ""}`}>
          <li>
            <Link to={"/user/cart"} href="#" className={styles.nav__link_cart}>
              <img src="/images/Cart.svg" alt="Cart Shop" />
            </Link>
          </li>
          <li>
            <Link to={"/"} href="#" className={styles.nav__link_kelas}>
              Kelasku
            </Link>
          </li>
          <li>
            <Link to={"/"} href="#" className={styles.nav__link_pembelian}>
              Pembelian
            </Link>
          </li>
          <li>|</li>
          <li>
            <Link to={"/"} href="#" className={styles.nav__link_person}>
              <img src="/images/Person.svg" alt="Cart Shop" />
            </Link>
          </li>
          <li>
            <Link to={"/"} href="#" className={styles.nav__link_logout}>
              <img src="/images/Logout.svg" alt="Cart Shop" />
            </Link>
          </li>
        </ul>
      </div>
    </nav>
  );
}
