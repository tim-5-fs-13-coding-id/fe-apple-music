import { Link } from "react-router-dom";
import styles from "./NavbarTransparent.module.scss";

export function NavbarTransparent() {
  return (
    <nav className={styles.header__nav}>
      <Link to={"/"}>
        <img src="/images/logo_apple_musik.png" alt="Logo apel musik" />
      </Link>
    </nav>
  );
}
