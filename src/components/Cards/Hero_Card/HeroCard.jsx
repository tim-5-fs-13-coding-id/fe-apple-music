import styles from "./HeroCard.module.scss";

export function HeroCard({ title, desc }) {
  return (
    <article className={styles.heroCard}>
      <h1>{title}</h1>
      <h2>{desc}</h2>
    </article>
  );
}
