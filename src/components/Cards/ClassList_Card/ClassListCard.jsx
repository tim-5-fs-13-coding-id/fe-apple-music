import styles from "./ClassListCard.module.scss";

export function ClassListCard({ imageLink, title }) {
  return (
    <article className={styles.classList_wrap}>
      <figure className={styles.image_wrap}>
        <img src={`/images/${imageLink}`} alt={title} />
      </figure>
      <div className={styles.description}>
        <h1 className={styles.title}>{title}</h1>
      </div>
    </article>
  );
}
