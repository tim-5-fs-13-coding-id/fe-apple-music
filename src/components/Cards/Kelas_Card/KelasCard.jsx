import styles from "./KelasCard.module.scss";

export function KelasCard({ image, classTitle, classDesc, price }) {
  return (
    <article className={styles.kelas_wrap}>
      <figure className={styles.image_wrap}>
        <img src={`/images/${image}`} alt={classTitle} />
      </figure>
      <div className={styles.description}>
        <h1 className={styles.title}>{classTitle}</h1>

        <p className={styles.desc}> {classDesc} </p>

        <h2 className={styles.price}>{price}</h2>
      </div>
    </article>
  );
}
