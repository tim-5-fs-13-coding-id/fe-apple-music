import styles from "./HeaderConfirm.module.scss";
import { Link } from "react-router-dom";

function HeaderConfirm() {
  return (
    <header className={styles.header}>
      <nav className="header__nav">
        <div className="header__nav-logo">
          <Link to={"/"}>
            <img src="/images/logo_apple_musik.png" alt="Logo apel musik" />
          </Link>
        </div>
      </nav>
    </header>
  );
}

export default HeaderConfirm