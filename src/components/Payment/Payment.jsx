import { Link } from "react-router-dom";
import styles from "./Payment.module.scss";
import { useState } from "react";

export function Payment({ handleModal }) {
  const [choosePyament, setChoosePayment] = useState(null);
  const [isChoosed, setIsChoosed] = useState(false);
  const [styleIndex, setStyleIndex] = useState(false);

  function handlePaymentSelect(paymentMethod, index) {
    setChoosePayment(paymentMethod);
    setStyleIndex(index);
    setIsChoosed(true);
  }

  console.log("payment: ", choosePyament);

  return (
    <aside className={styles.payment}>
      <h1>Pilih Metode Pembayaran</h1>
      <section className={styles.payment__method}>
        {listPayment.map((el, index) => (
          <div
            className={`${styles.wrapper} ${
              styleIndex === index ? styles[`active_${index + 1}`] : ""
            }`}
            key={el.name}
            onClick={() => handlePaymentSelect(el.name, index)}
          >
            <img src={`/images/${el.image}`} alt={el.name} />
            <h1>{el.name}</h1>
          </div>
        ))}
      </section>
      {isChoosed ? (
        ""
      ) : (
        <p className={styles.choose_first}>
          Silahkan memilih Payment terlebih dahulu
        </p>
      )}
      <section className={styles.payment__button}>
        <button className={styles.button_cancel} onClick={handleModal}>
          Batal
        </button>
        <Link
          to={`${isChoosed ? "/payment/success" : "/user/cart"}`}
          className={styles.button_pay}
        >
          Bayar
        </Link>
      </section>
    </aside>
  );
}

const listPayment = [
  {
    name: "Gopay",
    image: "gopay.png",
  },
  {
    name: "OVO",
    image: "ovo.png",
  },
  {
    name: "DANA",
    image: "dana.png",
  },
  {
    name: "Mandiri",
    image: "mandiri.png",
  },
  {
    name: "BCA",
    image: "bca.png",
  },
  {
    name: "BNI",
    image: "bni.png",
  },
];
