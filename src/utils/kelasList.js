export const kelasList = [
  {
    id: 1,
    kelasName: "Drum",
    link: "/kelas/drum",
    imageUrl: "drum.png",
  },
  {
    id: 2,
    kelasName: "Piano",
    link: "/kelas/piano",
    imageUrl: "Piano.png",
  },
  {
    id: 3,
    kelasName: "Gitar",
    link: "/kelas/gitar",
    imageUrl: "gitar.png",
  },
  {
    id: 4,
    kelasName: "Bass",
    link: "/kelas/bass",
    imageUrl: "bass.png",
  },
  {
    id: 5,
    kelasName: "Biola",
    link: "/kelas/biola",
    imageUrl: "biola_kelas.png",
  },
  {
    id: 6,
    kelasName: "Menyanyi",
    link: "/kelas/menyanyi",
    imageUrl: "menyanyi.png",
  },
  {
    id: 7,
    kelasName: "Flute",
    link: "/kelas/flute",
    imageUrl: "flute.png",
  },
  {
    id: 8,
    kelasName: "Saxophone",
    link: "/kelas/saxophone",
    imageUrl: "saxophone.png",
  },
];
