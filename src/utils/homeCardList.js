export const homeCardList = [
  {
    title: "500+",
    description:
      "Lebih dari sekedar kelas biasa yang bisa mengeluarkan bakat kalian",
  },
  {
    title: "50+",
    description: "Lulusan yang menjadi musisi ternama dengan skill memukau",
  },
  {
    title: "10+",
    description: "Coach Special kolaborasi dengan musisi terkenal",
  },
];
