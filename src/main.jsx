import React from "react";
import ReactDOM from "react-dom/client";
import "./index.scss";
import { RouterProvider, createBrowserRouter } from "react-router-dom";
import { Login } from "./pages/Auth/login/Login.jsx";
import { ForgetPassword } from "./pages/Auth/forget_password/ForgetPassword.jsx";
import Register from "./pages/Auth/register/Register.jsx";
import NewPassword from "./pages/Auth/new_password/NewPassword.jsx";
import { HomePage } from "./pages/Home/HomePage.jsx";
import { MenuCourse } from "./pages/MenuCourse/MenuCourse.jsx";
import { DetailCourse } from "./pages/DetailCourse/DetailCourse.jsx";
import { Cart } from "./pages/Cart/Cart.jsx";
import { PaymentSuccess } from "./pages/PaymentSuccess/PaymentSuccess.jsx";
import ConfirmRegister from "./pages/Auth/confirm_register/ConfirmRegister.jsx";

const router = createBrowserRouter([
  {
    path: "/",
    element: <HomePage />,
  },
  {
    path: "/login",
    element: <Login />,
  },
  {
    path: "/forgot_password",
    element: <ForgetPassword />,
  },
  {
    path: "/register",
    element: <Register />,
  },
  {
    path: "/confirm_register",
    element: <ConfirmRegister />
  },
  {
    path: "/new_password",
    element: <NewPassword />,
  },
  {
    path: "/kelas/:kelasId",
    element: <MenuCourse />,
  },
  {
    path: "/:kelasId/detail/:title_class",
    element: <DetailCourse />,
  },
  {
    path: "/user/cart",
    element: <Cart />,
  },
  {
    path: "/payment/success",
    element: <PaymentSuccess />,
  },
]);

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);
