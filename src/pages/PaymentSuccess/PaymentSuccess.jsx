import { Link } from "react-router-dom";
import { NavbarTransparent } from "../../components/Navbar/Navbar_Transparent/NavbarTransparent";
import styles from "./PaymentSuccess.module.scss";

export function PaymentSuccess() {
  return (
    <main className={styles.payment}>
      <NavbarTransparent />
      <div className={styles.body}>
        <div className={styles.body__image}>
          <img src="/images/payment_success_image.png" alt="" />
        </div>
        <div className={styles.body__message}>
          <h1>Pembelian Berhasil</h1>
          <p>Yey! Kamu telah berhasil membeli kursus di Apel Music</p>
        </div>
        <div className={styles.body__buttons}>
          <ul>
            <li>
              <Link to={"/"} className={styles.beranda}>
                <img src="/images/home_icon.svg" alt="Home Icon" />
                <span>Ke Beranda</span>
              </Link>
            </li>
            <li>
              <Link to={"/user/invoice"} className={styles.invoice}>
                <img src="/images/arrow_forward.svg" alt="Arraow Icon" />
                <span>Buka Invoice</span>
              </Link>
            </li>
          </ul>
        </div>
      </div>
    </main>
  );
}
