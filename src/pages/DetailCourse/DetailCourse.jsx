import { Header } from "../../components/Header/Header";
import styles from "./DetailCourse.module.scss";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import { Link } from "react-router-dom";
import { KelasFavoritList } from "../../utils/kelasFavoritList";
import { KelasCard } from "../../components/Cards/Kelas_Card/KelasCard";
import { Footer } from "../../components/Footer/Footer";
import { Routes, Route, useParams, useLocation } from "react-router-dom";
import { useEffect, useState } from "react";

export function DetailCourse() {
  //   const data = useParams();

  // Get Data from Link Before
  const { state } = useLocation();
  const [value, setValue] = useState("");

  let kelas = state.data;

  let getDrumAvailable = KelasFavoritList.filter(
    (el) => el.category === "Drum"
  ).filter((el) => el.id !== kelas.id);

  // Scroll to top
  const { pathname } = useLocation();
  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);

  function handleSelect(event) {
    setValue(event.target.value);
  }

  return (
    <main className={styles.detail_course}>
      <Header />

      <section className={styles.hero}>
        <div className={styles.product}>
          <figure className={styles.product__image}>
            <img
              src={`/images/${kelas.imageLink}`}
              alt={`${kelas.title}`}
              className={styles.product_image}
            />
          </figure>
          <div className={styles.product__caption}>
            <h1>{kelas.category}</h1>
            <h2> {kelas.title} </h2>
            <h3>{kelas.price}</h3>
            <FormControl
              sx={{ minWidth: 250 }}
              className={styles.select}
              size="small"
            >
              <InputLabel id="demo-simple-select-label">
                Pilih Jadwal Kelas
              </InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={value}
                label="Pilih Jadwal Kelas"
                onChange={handleSelect}
              >
                <MenuItem value={"Senin, 25 Juli 2022"}>
                  Senin, 25 Juli 2022
                </MenuItem>
                <MenuItem value={"Selasa, 26 Juli 2022"}>
                  Selasa, 26 Juli 2022
                </MenuItem>
                <MenuItem value={"Rabu, 27 Juli 2022"}>
                  Rabu, 27 Juli 2022
                </MenuItem>
                <MenuItem value={"Kamis, 28 Juli 2022"}>
                  Kamis, 28 Juli 2022
                </MenuItem>
                <MenuItem value={"Jumat, 29 Juli 2022"}>
                  Jumat, 29 Juli 2022
                </MenuItem>
                <MenuItem value={"Sabtu, 30 Juli 2022"}>
                  Sabtu, 30 Juli 2022
                </MenuItem>
              </Select>
            </FormControl>

            <div className={styles.button_wrap}>
              <Link to={"/user/cart"} className={styles.checkout}>
                Masukkan Keranjang
              </Link>
              <Link to={"/user/cart"} className={styles.purchase_now}>
                Beli Sekarang
              </Link>
            </div>
          </div>
        </div>
        <div className={styles.description}>
          <h1>Deskripsi</h1>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat. Duis aute irure dolor in
            reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
            pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
            culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum
            dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
            incididunt ut labore et dolore magna aliqua. Ut enim ad minim
            veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex
            ea commodo consequat. Duis aute irure dolor in reprehenderit in
            voluptate velit esse cillum dolore eu fugiat nulla pariatur.
            Excepteur sint occaecat cupidatat non proident, sunt in culpa qui
            officia deserunt mollit anim id est laborum.
          </p>
        </div>
      </section>

      <section className={styles.you_like}>
        <h1>Kelas lain yang mungkin kamu suka</h1>
        <div className={styles.explore}>
          {getDrumAvailable.map((kelas) => (
            <Link to={kelas.link} key={kelas.id} state={{ data: kelas }}>
              <KelasCard
                classDesc={kelas.title}
                classTitle={kelas.category}
                image={kelas.imageLink}
                price={kelas.price}
              />
            </Link>
          ))}
        </div>
      </section>

      <Footer />
    </main>
  );
}
