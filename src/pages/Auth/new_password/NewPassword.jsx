import { Link } from "react-router-dom";
import { Header } from "../../../components/Header/Header";
import styles from "./NewPassword.module.scss";
import { useState } from "react";

function NewPassword() {
  const [passwordIsNotValid, setpasswordIsNotValid] = useState(null);
  const [password, setPassword] = useState("");
  const [passwordConfirm, setPasswordConfirm] = useState(null);

  function handlePassword(e) {
    let value = e.target.value;

    if (value.length < 8) {
      setpasswordIsNotValid(true);
    } else {
      setpasswordIsNotValid(null);
    }

    setPassword(value);
  }

  function handlePasswordConfirm(e) {
    let value = e.target.value

    if (value !== password) {
      setPasswordConfirm(true)
    } else {
      setPasswordConfirm(null)
    }

  }

  return (
    <>
      <Header />
      <section className={styles.section}>
        <div className={styles.reset}>
          <h1>Buat Password</h1>
        </div>
        <div className={styles.input__wrap}>
          <div className={styles.input__wrap_element}>
            <input
              type="password"
              placeholder="Password Baru"
              onChange={handlePassword}
              className={passwordIsNotValid && styles.border_invalid}
            />
            {passwordIsNotValid && (
              <p className={styles.invalid}>Password kurang dari 8 karakter</p>
            )}
          </div>
          <div className={styles.input__wrap_element}>
            <input
              type="konfirmasi"
              placeholder="Konfirmasi Password Baru"
              onChange={handlePasswordConfirm}
              className={passwordConfirm && styles.border_invalid}
            />
            {passwordConfirm && (
              <p className={styles.invalid}>Password tidak sama</p>
            )}
          </div>
          <ul className={styles.input__wrap_nav}>
            <li>
              <Link to={"/login"} href="#" className={styles.cancel}>
                Batal
              </Link>
            </li>
            <li>
              <Link to={"/login"} href="#" className={styles.confirm}>
                Kirim
              </Link>
            </li>
          </ul>
        </div>
      </section>
    </>
  );
}

export default NewPassword