import { Link } from "react-router-dom";
import { Header } from "../../../components/Header/Header";
import styles from "./Register.module.scss"
import { useState } from "react";

function Register () {
  const [emailIsNotValid, setemailIsNotValid] = useState(null);
  const [passwordIsNotValid, setpasswordIsNotValid] = useState(null);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [passwordConfirm, setPasswordConfirm] = useState(null);

  function checkemailIsNotValid(value) {
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

    return emailRegex.test(value);
  }

  function handleEmail(e) {
    let value = e.target.value;

    if (!checkemailIsNotValid(value)) {
      setemailIsNotValid(true);
    } else {
      setemailIsNotValid(null);
    }

    setEmail(value);
  }

  function handlePassword(e) {
    let value = e.target.value;

    if (value.length < 8) {
      setpasswordIsNotValid(true);
    } else {
      setpasswordIsNotValid(null);
    }

    setPassword(value);
  }

  function handlePasswordConfirm(e) {
    let value = e.target.value

    if (value !== password) {
      setPasswordConfirm(true)
    } else {
      setPasswordConfirm(null)
    }

  }

  function handleClick() {
    if (!emailIsNotValid && !passwordIsNotValid && !passwordConfirm) {
      console.log("email: ", email);
      console.log("password: ", password);
      console.log("confirm: ", !passwordConfirm)
    }

    setEmail("");
    setPassword("");
    setPasswordConfirm("");
  }

    return (
        <>
            <Header />
            <section className={styles.section}>
            <div className={styles.welcome}>
                <h1>Selamat Datang Musikers!</h1>
                <h2>Yuk daftar terlebih dahulu akun kamu</h2>
            </div>
            <div className={styles.input_warp}>
                <div className={styles.input_warp__element}>
                    <input  
                    type="nama" 
                    placeholder="Masukkan Nama Lengkap"
                    />
                </div>
                <div className={styles.input_warp__element}>
                    <input 
                    type="email" 
                    placeholder="Masukkan Email" 
                    onChange={handleEmail}
                    className={emailIsNotValid && styles.border_invalid}
                    /> 
                    {emailIsNotValid && (
                        <p className={styles.invalid}>email tidak valid</p>
                    )}
                </div>
                <div className={styles.input_warp__element}>
                    <input 
                    type="password" 
                    placeholder="Masukkan Password" 
                    onChange={handlePassword}
                    className={passwordIsNotValid && styles.border_invalid} 
                    />
                    {passwordIsNotValid && (
                        <p className={styles.invalid}>Password kurang dari 8 karakter</p>
                    )}
                </div>
                <div className={styles.input_warp__element}>
                    <input  
                    type="konfirmasi" 
                    placeholder="Konfirmasi Password"
                    onChange={handlePasswordConfirm}
                    className={passwordConfirm && styles.border_invalid}
                    /> 
                    {passwordConfirm && (
                      <p className={styles.invalid}>Password tidak sama</p>
                    )}
                </div>
                <div className={styles.input_button}>
                    <Link to={"/confirm_register"} href="#" className={styles.signUp} onClick={handleClick}>
                    Daftar
                    </Link>
                    <p className={styles.login}>
                    Sudah punya akun? <Link to={"/login"} href="#">Login disini</Link>
                    </p>
                </div>
            </div>
            </section>
        </>
    ); 
}
export default Register