import { Link } from "react-router-dom";
import { Header } from "../../../components/Header/Header";
import styles from "./Login.module.scss";
import { useState } from "react";

export function Login() {
  const [emailIsNotValid, setemailIsNotValid] = useState(null);
  const [passwordIsNotValid, setpasswordIsNotValid] = useState(null);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  function checkemailIsNotValid(value) {
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

    return emailRegex.test(value);
  }

  function handleEmail(e) {
    let value = e.target.value;

    if (!checkemailIsNotValid(value)) {
      setemailIsNotValid(true);
    } else {
      setemailIsNotValid(null);
    }

    setEmail(value);
  }

  function handlePassword(e) {
    let value = e.target.value;

    if (value.length < 8) {
      setpasswordIsNotValid(true);
    } else {
      setpasswordIsNotValid(null);
    }

    setPassword(value);
  }

  function handleClick() {
    if (!emailIsNotValid && !passwordIsNotValid) {
      console.log("email: ", email);
      console.log("password: ", password);
    }

    setEmail("");
    setPassword("");
  }

  return (
    <>
      <Header />
      <section className={styles.section}>
        <div className={styles.welcome}>
          <h1>Selamat Datang Musikers!</h1>
          <h2>Login dulu yuk</h2>
        </div>
        <div className={styles.input_wrap}>
          <div className={styles.input_wrap__element}>
            <input
              type="email"
              placeholder="Masukkan Email"
              onChange={handleEmail}
              className={emailIsNotValid && styles.border_invalid}
            />
            {emailIsNotValid && (
              <p className={styles.invalid}>email tidak valid</p>
            )}
          </div>
          <div className={styles.input_wrap__element}>
            <input
              type="password"
              placeholder="Masukkan Password"
              onChange={handlePassword}
              className={passwordIsNotValid && styles.border_invalid}
            />
            {passwordIsNotValid && (
              <p className={styles.invalid}>Password kurang dari 8 karakter</p>
            )}
          </div>
          <Link
            to={"/forgot_password"}
            className={styles.forgot_password}
            href="#"
          >
            Lupa kata sandi
          </Link>
          <a href="#" className={styles.login} onClick={handleClick}>
            Masuk
          </a>
          <p className={styles.register}>
            Belum punya akun? <Link to={"/register"} href="#">Daftar disini</Link>
          </p>
        </div>
      </section>
    </>
  );
}
