import { Link } from "react-router-dom";
import { Header } from "../../../components/Header/Header";
import styles from "./ForgetPassword.module.scss";
import { useState } from "react";

export function ForgetPassword() {
  const [emailIsNotValid, setemailIsNotValid] = useState(null);
  const [email, setEmail] = useState("");

  function checkemailIsNotValid(value) {
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

    return emailRegex.test(value);
  }

  function handleEmail(e) {
    let value = e.target.value;

    if (!checkemailIsNotValid(value)) {
      setemailIsNotValid(true);
    } else {
      setemailIsNotValid(null);
    }

    setEmail(value);
  }

  return (
    <>
      <Header />
      <section className={styles.section}>
        <div className={styles.reset}>
          <h1>Reset Password</h1>
          <h2>Silahkan masukan terlebih dahulu email anda</h2>
        </div>
        <div className={styles.input__wrap}>
          <div className={styles.input__wrap_element}>
            <input
              type="email"
              placeholder="Masukkan Email"
              onChange={handleEmail}
              className={emailIsNotValid && styles.border_invalid}
            />
            {emailIsNotValid && (
              <p className={styles.invalid}>email tidak valid</p>
            )}
          </div>
          <ul className={styles.input__wrap_nav}>
            <li>
              <Link to={"/login"} href="#" className={styles.cancel}>
                Batal
              </Link>
            </li>
            <li>
              <Link to={"/new_password"} href="#" className={styles.confirm}>
                Konfirmasi
              </Link>
            </li>
          </ul>
        </div>
      </section>
    </>
  );
}
