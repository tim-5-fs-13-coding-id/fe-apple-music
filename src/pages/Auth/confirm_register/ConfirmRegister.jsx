import { Link } from "react-router-dom";
import HeaderConfirm from "../../../components/HeaderConfirm/HeaderConfirm";
import styles from "./ConfirmRegister.module.scss";

function ConfirmRegister () {

    return (

        <>
            <HeaderConfirm />  
            <section className={styles.section}>
                <div className={styles.images}>
                    <img src="/images/confirm.png" alt="Logo Confirmasi"></img>
                </div>
                <div className={styles.email_confirm}>
                    <h1>Konfirmasi Email Berhasil</h1>
                    <p>Silahkan Login terlebih dahulu untuk masuk ke aplikasi</p>
                </div>
                <div className={styles.tombol}> 
                    <Link to={"/login"}>
                        Masuk Sekarang
                    </Link>
                </div>
            </section> 
        </>

    );

}

export default ConfirmRegister