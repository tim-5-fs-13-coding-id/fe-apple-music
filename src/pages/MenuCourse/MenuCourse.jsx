import { Link } from "react-router-dom";
import { Header } from "../../components/Header/Header";
import { KelasFavoritList } from "../../utils/kelasFavoritList";
import styles from "./MenuCourse.module.scss";
import { KelasCard } from "../../components/Cards/Kelas_Card/KelasCard";
import { Footer } from "../../components/Footer/Footer";
import { useEffect } from "react";
import { useLocation } from "react-router-dom";

export function MenuCourse() {
  let getDrumAvailable = KelasFavoritList.filter(
    (el) => el.category === "Drum"
  );

  // Scroll to top
  const { pathname } = useLocation();

  // Automatically scrolls to top whenever pathname changes
  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);

  return (
    <main className={styles.main}>
      <Header />

      <section className={styles.hero}>
        <img
          src="/images/banner_detail_kelas.png"
          alt="Banner Hero image "
          className={styles.hero__image}
        />
        <div className={styles.description}>
          <h1>Drummer Class</h1>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat. Duis aute irure dolor in
            reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
            pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
            culpa qui officia deserunt mollit anim id est laborum.
          </p>
        </div>
      </section>

      <section className={styles.available}>
        <h1>Kelas yang tersedia</h1>

        <div className={styles.explore}>
          {getDrumAvailable.map((kelas) => (
            <Link to={kelas.link} state={{ data: kelas }} key={kelas.id}>
              <KelasCard
                classDesc={kelas.title}
                classTitle={kelas.category}
                image={kelas.imageLink}
                price={kelas.price}
              />
            </Link>
          ))}
        </div>
      </section>

      <Footer />
    </main>
  );
}
