import { Header } from "../../components/Header/Header";
import Box from "@mui/material/Box";
import Checkbox from "@mui/material/Checkbox";
import FormControlLabel from "@mui/material/FormControlLabel";
import { useEffect, useState } from "react";
import styles from "./Cart.module.scss";
import { DetailProduct } from "../../components/Cart/DetailProduct";
import { cartList } from "../../utils/cartList";
import { Payment } from "../../components/Payment/Payment";

export function Cart() {
  const [cartItems, setCartItems] = useState(cartList);
  const [pay, setPay] = useState(false);

  function handleChecked(index) {
    setCartItems((prevCartItems) => {
      const newCartItems = [...prevCartItems];
      newCartItems[index] = {
        ...newCartItems[index],
        checked: !newCartItems[index].checked,
      };

      return newCartItems;
    });
  }

  const handleSelectAll = () => {
    setCartItems((prevCartItems) =>
      prevCartItems.map((item) => ({
        ...item,
        checked: !prevCartItems[0].checked,
      }))
    );
  };

  const handleDelete = (id) => {
    console.log(id);
    setCartItems((prevCartItems) => {
      const newcartItems = prevCartItems.filter((item) => item.id !== id);

      return newcartItems;
    });
  };

  const handleModal = () => {
    setPay((prevState) => !prevState);
  };

  const areAllSelected = cartItems.every((item) => item.checked);

  //   console.log(checked);
  console.log(cartItems);

  const formatPrice = (price) => {
    return new Intl.NumberFormat("id-ID", {
      style: "currency",
      currency: "IDR",
      minimumFractionDigits: 0,
    }).format(price);
  };

  const children = (
    <Box sx={{ display: "flex", flexDirection: "column", ml: 3 }}>
      {cartItems.map((data, index) => (
        <div className={styles.checkbox__wrapper} key={data.id}>
          <div className={styles.custom}>
            <FormControlLabel
              label={
                <DetailProduct
                  category={data.category}
                  image={data.imageLink}
                  jadwal={data.jadwal}
                  title={data.title}
                  price={formatPrice(data.price)}
                />
              }
              control={
                <Checkbox
                  checked={data.checked}
                  onChange={() => handleChecked(index)}
                />
              }
            />
            <button
              className={styles.button_delete}
              onClick={() => handleDelete(data.id)}
            >
              {" "}
              <img src="/images/delete_forever.svg" alt="" /> <p>delete</p>
            </button>
          </div>
          <div className={styles.garis}></div>
        </div>
      ))}
    </Box>
  );

  return (
    <main className={styles.cart}>
      <Header />
      <section className={styles.product__list}>
        <FormControlLabel
          label="Pilih Semua"
          className={styles.label}
          control={
            <Checkbox checked={areAllSelected} onChange={handleSelectAll} />
          }
        />
        <div className={styles.garis_atas}></div>
        {children}
      </section>

      <section className={styles.end_line}></section>

      <section className={styles.total_price}>
        <div className={styles.price}>
          <h1>Total Hara</h1>
          <h2>
            {cartItems
              .filter((item) => item.checked)
              .reduce((total, item) => total + item.price, 0)
              .toLocaleString("id-ID", {
                style: "currency",
                currency: "IDR",
                minimumFractionDigits: 0,
              })}
          </h2>
        </div>
        <button className={styles.button} onClick={handleModal}>
          Bayar Sekarang
        </button>
      </section>

      {/*------------- Start of Modal Popup Payment ----------------  */}
      {pay && <section className={styles.modal_payment}></section>}

      {pay && (
        <div className={styles.payment_method}>
          <Payment handleModal={handleModal} />
        </div>
      )}

      {/*------------- End of Modal Popup Payment ----------------  */}
    </main>
  );
}
