import { Link } from "react-router-dom";
import { HeroCard } from "../../components/Cards/Hero_Card/HeroCard";
import { KelasCard } from "../../components/Cards/Kelas_Card/KelasCard";
import { Header } from "../../components/Header/Header";
import { homeCardList } from "../../utils/homeCardList";
import { KelasFavoritList } from "../../utils/kelasFavoritList";
import styles from "./HomePage.module.scss";
import { ClassListCard } from "../../components/Cards/ClassList_Card/ClassListCard";
import { kelasList } from "../../utils/kelasList";
import { Footer } from "../../components/Footer/Footer";
import { useLocation } from "react-router-dom";
import { useEffect } from "react";
import { useMediaQuery } from "../../hooks/useMediaQuery";
import { NavbarAuthenticate } from "../../components/Navbar/Navbar_Authenticate/NavbarAuthenticate";

export function HomePage() {
  // Scroll to top
  const { pathname } = useLocation();
  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);

  // const isMobile = useMediaQuery("(max-width: 768px)");
  // console.log(isMobile);

  return (
    <main className={styles.main}>
      <NavbarAuthenticate />
      <section className={styles.hero}>
        <img
          src="/images/hero_landing_page.png"
          alt="Hero Image"
          className={styles.hero__image}
        />

        <div className={styles.hero__content}>
          <h1 className={styles.heading1}>
            Hi Musiker! Gabung yuk di Apel Music
          </h1>
          <h2 className={styles.heading2}>
            Banyak kelas keren yang bisa menunjang bakat bermusik kamu
          </h2>
          <aside className={styles.hero_card}>
            {homeCardList.map((el) => (
              <div key={el.title}>
                <HeroCard title={el.title} desc={el.description} />
              </div>
            ))}
          </aside>
        </div>
      </section>
      <section className={styles.explore_kelas}>
        <div className={styles.head}>
          <h1>Explore kelas favorit</h1>
        </div>
        <div className={styles.explore}>
          {KelasFavoritList.map((kelas) => (
            <Link to={kelas.link} key={kelas.id} state={{ data: kelas }}>
              <KelasCard
                classDesc={kelas.title}
                classTitle={kelas.category}
                price={kelas.price}
                image={kelas.imageLink}
              />
            </Link>
          ))}
        </div>
      </section>
      <section className={styles.list_kelas}>
        <div className={styles.head}>
          <h1>Pilih kelas impian kamu</h1>
        </div>
        <div className={styles.explore}>
          {kelasList.map((kelas) => (
            <Link to={kelas.link} key={kelas.id}>
              <ClassListCard
                imageLink={kelas.imageUrl}
                title={kelas.kelasName}
              />
            </Link>
          ))}
        </div>
      </section>
      <section className={styles.benefit}>
        <div className={styles.images}>
          <img
            src="/images/Bulat.png"
            alt="Bulang doang ga penting"
            className={styles.bulat}
          />
          <img
            src="/images/orang_make_gitar.png"
            alt="orang make gitar"
            className={styles.orang_gitar}
          />
        </div>
        <div className={styles.brand}>
          <h1 className={styles.title}>Benefit ikut Apel Course</h1>
          <p className={styles.desc}>
            Sed ut perspiciatis unde omnis iste natus error sit voluptatem
            accusantium doloremque laudantium, totam rem aperiam, eaque ipsa
            quae ab illo inventore veritatis et quasi architecto beatae vitae
            dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit
            aspernatur aut odit aut fugit, sed quia consequuntur magni dolores
            eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est,
            qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit,
            sed quia non numquam eius modi tempora incidunt ut labore et dolore
            magnam aliquam quaerat voluptatem.
          </p>
        </div>
      </section>
      <Footer />
    </main>
  );
}
